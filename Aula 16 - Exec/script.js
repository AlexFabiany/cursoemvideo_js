let num = document.querySelector('input#txtnum');
let lista = document.querySelector('select#selnums');
let res = document.querySelector('div#res');
let numeros = [];

function isNumero(num) {
  if(Number(num) >= 1 && Number(num) <= 100) {
    return true;
  } 
  else {
    return false;
  }
}

function inLista(num, lista) {
  if(lista.indexOf(Number(num)) != -1) {
    return true;
  } else {
    return false;
  }
}

function adicionar() {
  if(isNumero(num.value) && !inLista(num.value, numeros)) {
    numeros.push(Number(num.value));

    let item = document.createElement('option');
    item.text = `Adicionado -> ${num.value}`;
    lista.appendChild(item);
    res.innerHTML = '';    
  } else {

  }
  num.value = '';
  num.focus();
}

function analisar() {
  if(numeros.length === 0){

  }
  let total = numeros.length;
  let menor = numeros[0];
  let maior = numeros[0];
  let soma = 0;
  let media = 0;
  for(let pos in numeros) {
    soma += numeros[pos];
    if(numeros[pos] < menor) {
      menor = numeros[pos];
    }
    if(numeros[pos] > maior) {
      maior = numeros[pos];
    }
  }
  media = soma / total;
  res.innerHTML = '';
  res.innerHTML += `<p>Ao todos temos ${total} números adicionados.</p>`;
  res.innerHTML += `<p>O menor números é o ${menor}.</p>`;
  res.innerHTML += `<p>O maior números é o ${maior}.</p>`;
  res.innerHTML += `<p>A soma dos números é  ${soma}.</p>`;
  res.innerHTML += `<p>A média dos números é  ${media.toFixed(2)}.</p>`;
}