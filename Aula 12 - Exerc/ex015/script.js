function verificar() {
  var data = new Date();
  var ano = data.getFullYear();
  var fano = document.getElementById('txtano');
  var res = document.querySelector('div#res');

  if(!fano.value || Number(fano.value) > ano) {
    res.innerHTML = '[ERRO] Verifique os dados e tente novamente!';
  } else {
    var fsex = document.getElementsByName('radsex');
    var idade = ano - Number(fano.value);
    //res.innerHTML = `Idade calculada: ${idade}`;
    var genero = '';
    var faixa = '';
    var img = document.createElement('img');
    img.setAttribute('id', 'foto');

    if(fsex[0].checked) {
      genero = 'Homem';
      if(idade >=0 && idade < 13){
        // Criança
        faixa = 'Criança';
        img.setAttribute('src', 'img/bebemasculino.png');
      } else if(idade < 25) {
        // Jovem
        faixa = 'Jovem';
        img.setAttribute('src', 'img/jovemmasculino.png');
      } else if(idade < 61) {
        // Adulto
        faixa = 'Adulto';
        img.setAttribute('src', 'img/adultomasculino.png');
      } else {
        // Idoso
        faixa = 'Idoso';
        img.setAttribute('src', 'img/idosomasculino.png');
      }
    } else {
      genero = 'Mulher';
      if(idade >=0 && idade < 13){
        // Criança
        faixa = 'Criança';
        img.setAttribute('src', 'img/bebefeminino.png');
      } else if(idade < 25) {
        // Jovem
        faixa = 'Jovem';
        img.setAttribute('src', 'img/jovemfeminino.png');
      } else if(idade < 61) {
        // Adulto
        faixa = 'Adulta';
        img.setAttribute('src', 'img/adultofeminino.png');
      } else {
        // Idoso
        faixa = 'Idosa';
        img.setAttribute('src', 'img/idosofeminino.png');
      }
    }
    res.style.textAlign = 'center';
    res.innerHTML = `Detectado: ${genero} - ${faixa} com idade de ${idade} anos`;
    res.appendChild(img);
  }
}