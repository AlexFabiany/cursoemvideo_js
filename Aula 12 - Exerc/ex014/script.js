function carregar() {
  var msghora = document.getElementById('hora');
  var img = document.getElementById('imagem');
  var msg = document.getElementById('msg');
  var data = new Date();
  var hora = data.getHours();
  var min = data.getMinutes();

  msghora.innerHTML = `Agora são ${hora}:${min}h`;

  if(hora > 0 && hora < 12){
    // Bom dia
    img.src = 'img/fotomanha.png';
    document.body.style.background = '#75c2f9';
    msg.innerHTML = `Bom dia!"`;
  } else if (hora >= 12 && hora < 18) {
    // Boa tarde
    img.src = 'img/fototarde.png';
    document.body.style.background = '#ac6f6b';
    msg.innerHTML = `Boa tarde!`;
  } else {
    // Boa noite
    img.src = 'img/fotonoite.png';
    document.body.style.background = '#2b3945';
    msg.innerHTML = `Boa noite!`;
  }
}
