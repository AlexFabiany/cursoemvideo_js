console.log("--- WHILE ---");
var c = 1;
while(c <= 5) {
  console.log(`Passo ${c} ...`);
  c++;
}

console.log("--- DO..WHILE ---");
var c = 1;

do {
  console.log(`Passo ${c} ...`);
  c++;
} while(c <= 5)