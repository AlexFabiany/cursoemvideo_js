function parimp(n) {
  if(n%2 == 0) {
    return 'Par';
  } else {
    return 'Impar';
  }
}
console.log(parimp(23));

function fatorial(n) {
  let fat = 1;
  if(n == 1 || n == 0) return 1
  else if(n < 0) return -1
  else {
    for(let c = n; c > 1; c--){
      fat *= c;
    }
  }
  return fat;
}
console.log(fatorial(6));

// Recursividade
function fatorial(n) {
  if(n == 1 || n == 0) return 1
  else if(n < 0) return -1
  else {
    return n * fatorial(n-1);
  }
}
console.log(fatorial(5));