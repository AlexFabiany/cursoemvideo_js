function gerarTabuada() {
  let num = document.querySelector('#txtnum');
  let tab = document.querySelector('#seltab');
  let op = document.getElementsByName('operacao');
  
  if(!num.value) {
    tab.innerHTML = '';
    let item = document.createElement('option');
    item.text = 'Por favor digite um múmero acima!';
    tab.appendChild(item);
  } else {
    tab.innerHTML = '';
    let operacao = '';
    if(op[0].checked) {
      operacao = '+';
    } else if(op[1].checked) {
      operacao = '-';
    } else if(op[2].checked) {
      operacao = '*';
    } else {
      operacao = '/';
    }
    let n = Number(num.value);
    for (let c = 0; c < 10; c++) {
      let item = document.createElement('option');
      item.value = `tab${c + 1}`;
      item.text = `${n} ${operacao} ${c + 1} = `;
      
      if(op[0].checked) {
        item.text += ` ${n + (c + 1)}`;
      } else if(op[1].checked) {
        item.text += ` ${n - (c + 1)}`;
      } else if(op[2].checked) {
        item.text += ` ${n * (c + 1)}`;
      } else {
        item.text += ` ${n / (c + 1)}`;
      }
      
      tab.appendChild(item);
    }
  }


}