function contar() {
  let ini = document.querySelector('#txtini');
  let fim = document.querySelector('#txtfim');
  let passo = document.querySelector('#txtpasso');
  let res = document.querySelector('#res');

  if(!ini.value || !fim.value || !passo.value) {
    res.innerHTML = '<strong>Faltam informações. verifique...</strong>';
  } else {
    res.innerHTML = 'Contando: <br>';

    let i = Number(ini.value);
    let f = Number(fim.value);
    let p = Number(passo.value);

    if(p <= 0){
      res.innerHTML += 'Considerando Passo 1... <br>';
      p = 1;
    }

    if(i < f) {
      // Contagem crescente
      for(var c = i; c <= f; c += p) {
        res.innerHTML += `\u{1F449} ${c} `; // https://unicode.org/emoji/charts/full-emoji-list.html
      }
    } else {
      // Contagem regressiva
      for(let c = i; c >= f; c -= p) {
        res.innerHTML += `\u{1F449} ${c} `;
      }
    }
    res.innerHTML += `\u{1F3F3}\u{FE0F}\u{200D}\u{1F308}`;

    // U+1F3F3 U+FE0F U+200D U+1F308
  }
}