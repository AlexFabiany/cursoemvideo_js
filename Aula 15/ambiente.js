let num = [5, 8, 3, 9, 2];

console.log(`Nosso vetor original é o ${num}`)

num.push(1);

console.log(`Nosso vetor agora é ${num}, com tamanho ${num.length}`)

console.log(`Nosso vetor ordenado é ${num.sort()}`)
let vetor = '';
for (let pos = 0; pos < num.length; pos++) {
  vetor += `${num[pos]}`;
  if(pos < num.length) vetor += `, `;
}
console.log(vetor);

for (let pos in num) {
  console.log(`A posição ${pos} tem o valor ${num[pos]}`);
  
}